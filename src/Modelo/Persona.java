/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Persona {
    
    private int cedula;
    private String nombre;
    private short dia, mes, agno;
    private String email;
    private short id_centro;

    public Persona() {
    }

    public Persona(int cedula, String nombre, short dia, short mes, short agno, String email, short id_centro) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.dia = dia;
        this.mes = mes;
        this.agno = agno;
        this.email = email;
        this.id_centro = id_centro;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getDia() {
        return dia;
    }

    public void setDia(short dia) {
        this.dia = dia;
    }

    public short getMes() {
        return mes;
    }

    public void setMes(short mes) {
        this.mes = mes;
    }

    public short getAgno() {
        return agno;
    }

    public void setAgno(short agno) {
        this.agno = agno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public short getId_centro() {
        return id_centro;
    }

    public void setId_centro(short id_centro) {
        this.id_centro = id_centro;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", dia=" + dia + ", mes=" + mes + ", agno=" + agno + ", email=" + email + ", id_centro=" + id_centro + '}';
    }
    
    
    
}
