/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class Departamento {

    private short id_dpto;
    private String nombre;
    private ListaCD<Municipio> municipios;

    public Departamento() {
        
    }

    public Departamento(short id_dpto, String nombre) {
        this.id_dpto = id_dpto;
        this.nombre = nombre;
        this.municipios = new ListaCD();
    }

    public short getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(short id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaCD<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ListaCD<Municipio> municipios) {
        this.municipios = municipios;
    }

    public void insertarMunicipio(Municipio nuevo) {
        this.municipios.insertarFin(nuevo);
    }

    @Override
    public String toString() {
        String msg = "Nombre Dpto:" + this.getNombre();
        for (Municipio m : this.getMunicipios()) {
            msg += "\n" + m.toString();
        }
        return msg;
    }

}
