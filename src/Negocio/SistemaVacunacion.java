/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Departamento;

/**
 *
 * @author madar
 */
public class SistemaVacunacion {

    private Departamento[] dptos;

    public SistemaVacunacion() {
    }

    public SistemaVacunacion(String url1, String url2, String url3) {

        this.dptos = new Departamento[32];

        this.crearDptos(url1);
        this.crearMunicipio(url2);
        this.crearPersonas(url3);

    }

    private void crearDptos(String url) {

    }

    private void crearMunicipio(String url) {

    }

    private void crearPersonas(String url) {

    }

    @Override
    public String toString() {
        String msg = "";
        for (Departamento d : this.dptos) {
            msg += "\n" + d.toString();
        }
        return msg;
    }

}
